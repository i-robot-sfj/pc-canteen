const IS_PROD = process.env.NODE_ENV === 'production';

let outputDir = process.env.OUTPUT_DIR || 'dist';
if (process.env['npm_lifecycle_event'].endsWith(':test')) {
	outputDir += '-test';
}

module.exports = {
	publicPath: IS_PROD ? `/${outputDir}` : './',
	outputDir,
	assetsDir: 'static',
	lintOnSave: false,
	runtimeCompiler: true, // 是否使用包含运行时编译器的 Vue 构建版本
	productionSourceMap: !IS_PROD, // 生产环境的 source map
	parallel: require('os').cpus().length > 1,
	devServer: {
		historyApiFallback: true,
		proxy: {
			"/api": {
				target: "https://sfjucanteen.51canteen.com/net",
				changeOrigin: true,
			},
		},
	},
	configureWebpack: {
		resolve: {
			fallback: {
				path: require.resolve('path-browserify'),
			},
		},
	},
};
