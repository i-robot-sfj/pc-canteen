//编辑消费机-空数据模板
export let temp={
    company_id:'',
    canteen_id:'',  
    province:'',
    city:'',
    devises:[
        {
            devise_name:'',
            devise_num:'',
            devise_ac:'',
        }
    ],
    typeChecked:[],//消费方式
    offlineCall:false,//离线提醒
    receivers:[],//离线接收人


    callSucessScreen:'1',//扣费成功-界面
    callSucessVoiceType:'0',//扣费成功-语音
    customSucessVoice:'',//扣费成功-自定义语音
    voiceSucessChecked:[],//扣费成功-附加语音

    callFailScreen:'1',//扣费失败-界面
    callFailVoiceType:'0',//扣费成功-语音
    customFailVoice:'',//扣费成功-自定义语音
    voiceFailChecked:[],//扣费成功-附加语音

    acknowledgeTxt:'无法识别',//无法识别-文本
    acknowledgeVoiceType:'0',//无法识别-类别
    acknowledgeVoiceTxt:'',//无法识别-语音内容
}