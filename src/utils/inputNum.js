export function clearNoNum(event, obj) {
    //响应鼠标事件，允许左右方向键移动
    event = window.event || event;
    if (event.keyCode == 37 | event.keyCode == 39) {
        return;
    }
    var t = obj.value.charAt(0);
    //先把非数字的都替换掉，除了数字和.
    obj.value = obj.value.replace(/[^\d.]/g, "");
    //必须保证第一个为数字而不是.
    obj.value = obj.value.replace(/^\./g, "");
    //保证只有出现一个.而没有多个.
    obj.value = obj.value.replace(/\.{2,}/g, ".");
    //保证.只出现一次，而不能出现两次以上
    obj.value = obj.value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
    //如果第一位是负号，则允许添加   如果不允许添加负号 可以把这块注释掉
    if (t == '-') {
        obj.value = '-' + obj.value;
    }
}
// 额外费用校验输入正负数， 保留2位小数 调用公共方法
export function RestrictedMoney(values) {
    return plusOrMinus(values.toString());
}
// 结合change事件对失去焦点进行判断，防止输入一些无效值
export function materielExtraCostChange(item) {
    // 防止删除为空
    if (!item) {
        item = '0.00';
    }
    // 一些错误金额输入的判断
    if (item.toString().indexOf('.') > 0 && Number(item.toString().split('.')[1].length) < 1) {
        item = item.toString().split('.')[0];
    }
    // 一些错误金额输入的判断
    if (!item || item === '-' || item === '-0') {
        item = '0.00';
        return;
    }
    // item = parseFloat(item).toFixed(2);
    item = parseFloat(item)
    return plusOrMinus(item)
    // return item
}
export function plusOrMinus(values) {
    let newValue;
    if (!(/[^0-9.-]/g.test(values))) {
        newValue = values.replace(/[^\-\d.]/g, '').replace(/\-{2,}/g, '-').replace(/\-{2,}/g, '-').replace(/^\./g, '')
            .replace(/\.{2,}/g, '.')
            .replace('.', '$#$')
            .replace(/\./g, '')
            .replace('$#$', '.');
        if (newValue.toString().indexOf('.') > 0 && Number(newValue.toString().split('.')[1].length) > 2) {
            newValue = parseInt(parseFloat(newValue) * 100) / 100;
        }
        if ((newValue.toString().split('-').length - 1) > 1) {
            newValue = parseFloat(newValue) || '';
        }
        if ((newValue.toString().split('-').length) > 1 && newValue.toString().split('-')[0].length > 0) {
            newValue = parseFloat(newValue) || '';
        }
        if (newValue.toString().length > 1 && (newValue.toString().charAt(0) === '0' || (newValue.toString().length > 2 && newValue.toString().charAt(0) === '-' && newValue.toString().charAt(1) === '0' && newValue.toString().charAt(2) !== '.')) && newValue.toString().indexOf('.') < 1) {
            newValue = parseFloat(newValue) || '';
        }
        // 判断整数位最多为9位
        if (newValue.toString().indexOf('.') > 0 && Number(newValue.toString().split('.')[0].length) > 9) {
            newValue = `${newValue.toString().substring(0, 9)}.${newValue.toString().split('.')[1]}`;
        } else if (newValue.toString().indexOf('.') < 0 && Number(newValue.toString().split('.')[0].length) > 9) {
            newValue = newValue.toString().substring(0, 9);
        }
    } else {
        newValue = values.replace(/[^0-9.-]/g, '');
    }
    console.log('newValue', newValue)
    return newValue;
    // return materielExtraCostChange(newValue)
}