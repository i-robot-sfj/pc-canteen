import axios from 'axios'
const allowIP = ['120.236.247.210', '116.27.120.144','183.236.246.246','19.52.81.59','116.27.122.224','119.133.5.19','14.123.192.27','14.123.193.154','27.41.99.180','117.71.138.37','117.71.136.201','218.104.196.98',]; //允许访问 伯宏 良哥（2） 横琴饭堂
async function getIp() {
    
    try {
        const res=await axios.get('https://api.ipify.org/') //原地址失效
        success(res);
        return res.data
    } catch(e) {
        //当接口不通时，随便返回一个不在白名单中的ip，防止接口维护未返回ip导致后续判断错误造成堵塞
        console.log(e);
        return '10.100.100.100'
    }
    // const res=await axios.get('http://pv.sohu.com/cityjson?ie=utf-8')  //新的搜狗地址
}

//如果url携带了参数，再执行获取key参数的方法
if(window.location.href.includes('?')){
    var url = window.location.href ;             //获取当前url
    var dz_url = url.split('#')[0];                //获取#/之前的字符串
    var cs = dz_url.split('?')[1];                //获取?之后的参数字符串
    var cs_arr = cs.split('&');                    //参数字符串分割为数组
    var cs={};
    for(var i=0;i<cs_arr.length;i++){         //遍历数组，拿到json对象
        cs[cs_arr[i].split('=')[0]] = cs_arr[i].split('=')[1]
    }
    //当key参数存在的情况才会进行储存，否则会出错
    //将拿到的key放置到本地储存中，不会随着刷新而变化（注意点，第一次凭借key进入系统并且成功登录之后，假如退出登录，key将被清除，因为退出登录的方法当中使用了localstorage.clear()）
    if(cs.key){
        localStorage.setItem("key",cs.key);
        console.log('输入的key',cs.key)
    } 
}

//key程序（横琴环境专用）      用途：当横琴的饭堂ip变化了不在网段之内，可以通过key程序临时让他们进入系统，具体流程见交接文档
async function backDoor(){
    let fd=new FormData()
    // let key=localStorage.getItem("key")
    fd.append("key",localStorage.getItem("key"))
    // for (var value of fd.values()) {
    //     console.log('fd.key',value);
    // }
    const res=await axios.post('https://rmyydjango.51canteen.com/master_key/verify_key',fd)
    // console.log('res.data',res.data)
    return res
}

//主程序
async function isAllow(){
    //s部署正式系统时开启
    let isAllow=true
    //e部署正式系统时开启
    
    //s部署横琴系统时开启
    // let isAllow=false
    // let ip=await getIp() 
    // //对固定ip进行过滤
    // for(let i=0; i<allowIP.length; i++){
    //     if (ip == allowIP[i]){
    //         isAllow=true
    //         console.log('在固定ip名单中')
    //         break;
    //     }
    // }

    // // //对固定网段进行过滤(横琴的网段14.123.19X.X)
    // let pattern = /14\.123\.19[0-9]\.\d{1,3}/,
    // pattern_weilun = /14\.117\.9[0-9]\.\d{1,3}/,//伟伦工作室的网段
	// str = typeof(ip)=='number' ? (ip).toString() : ip;
    // console.log(pattern.test(str));
    // if(pattern.test(str) || pattern_weilun.test(str)){
    //     isAllow=true
    //     console.log('在固定网段中')
    // }
    // // 当ip认证未通过时，再启用key认证
    // if(isAllow==false&&localStorage.getItem("key")){
    //     const res=await backDoor()
    //     if(res.data.msg=='ok'){
    //         isAllow=true
    //     }
    // }
    //e部署横琴系统时开启

    return isAllow
}




export default isAllow