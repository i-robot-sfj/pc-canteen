import $axios from "@/api/newIndex";
import { Message } from "element-ui";
// import initWebsocket from '@/utils/createWebSocket'
import store from "@/store/index";
var ws = null;

var wsUrl = process.env.VUE_APP_WSSURL
function openDownloadDialog(url){  
    store.dispatch("user/_getLogs")
    window.open(url);
    Message.success('导出成功！')
    ws.close()
}

export default async function(url, params,method){
    
    ws = new WebSocket(wsUrl);
    ws.onopen = function () {
        console.log("连接成功"+new Date().toUTCString());
    };
    ws.onmessage = async function (event) {
        // 获取数据
        let data=JSON.parse(event.data)
        console.log(data);
        if(data.msg=='success'&&data.type=='init'){
            let client_id=data.data.client_id
            const res = await $axios.post('/v1/token/admin/bind', {client_id});
            if(res.msg=='ok'){
                console.log('绑定成功')
            }
        }else if(data.type=='down_excel'){
            openDownloadDialog(data.url)
            
        }
    }
    ws.onclose = function () {
        console.log("连接关闭!"+new Date().toUTCString());
    };
    ws.onerror = function () {
        console.log("连接错误!");
    };

    let timer=null
    //默认使用get
    let res
    if(method==undefined){
        res = await $axios.get(url, params);
    }else{
        if(method=='get'){
            res = await $axios.get(url, params);
        }else if(method=='post'){
            res = await $axios.post(url, params);
        }
    }
    if (res.msg == "ok") {
        this.$store.dispatch("user/_getLogs")
        this.$message({
            dangerouslyUseHTMLString: true,
            message: '<div style="text-align:center"><p>导出中，请稍等...</p><p style="margin-top:8px">（可在导出记录查看进度）</p></div>'
        });
    } else {
        Message.error(res.msg);
    }
    
}