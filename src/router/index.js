import Vue from "vue";
import Router from "vue-router";
import store from "@/store";

Vue.use(Router);
import Layout from "@/layout";
import isAllow from '../utils/getIp'
const IS_UPGRADE=false //是否为维护模式

/**
 * 路由相关属性说明
 * hidden: 当设置hidden为true时，意思不在sideBars侧边栏中显示
 * mete{
 * title: xxx,  设置sideBars侧边栏名称
 * icon: xxx,  设置ideBars侧边栏图标
 * noCache: true  当设置为true时不缓存该路由页面
 * }
 */

/*通用routers*/

export const currencyRoutes = [
  {
    path: "/power",
    component: () => import("@/views/power"),
    hidden: true,
    meta: {
      title: '权限'
    }
  },
  {
    path: "/test",
    component: () => import("@/views/test"),
    hidden: true,
    meta: {
      title: '测试'
    }
  },
  {
    path: '/takeout/print',
    component: () => import("@/views/print"),
    hidden: true,
    meta: {
      title: '打印'
    }
  },
 
  {
    path: "/",
    redirect: IS_UPGRADE?"/debug":"/login"
  },
  {
    path: "/debug",
    name: "Debug",
    component: () => import("@/views/debug"),
    meta: {
      title: "维护页面"
    },
    hidden: true
  },
  {
    path: "/debuglogin",
    name: "DebugLogin",
    component: () => import("@/views/debug/debugLogin"),
    meta: {
      title: "维护页面"
    },
    hidden: true
  },
  {
    path: "/404",
    component: () => import("@/views/error"),
    hidden: true
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("@/views/login"),
    meta: {
      title: "登陆页面"
    },
    hidden: true
  },
  {
    path: "/producer",
    name: "producerLogin",
    component: () => import("@/views/producerLogin"),
    meta: {
      title: "登陆页面"
    },
    hidden: true
  },
  {
    path: "/order",
    name: "Order",
    redirect: "/order/order-statistics",
    component: Layout,
    meta: {
      title: "订餐管理",
      icon: "el-icon-document"
    },
    hidden: true,
    children: [
      {
        path: "order-details",
        name: "Order-details",
        component: () => import("@/views/orderManage/Details"),
        meta: {
          title: "订餐明细"
        },
        hidden: true
      }, {
        path: "order-statistics",
        name: "Order-statistics",
        component: () => import("@/views/orderManage/Statistics"),
        meta: {
          title: "订餐统计"
        },
        hidden: true
      },
    ]
  },
];
export const producerRoutes = [
  {
    path: "/canteen",
    name: "Canteen",
    redirect: "/canteen/manage",
    component: Layout,
    meta: {
      title: "小卖部管理",
      icon: "el-icon-coffee"
    },
    children: [
      {
        path: "manage",
        name: "Canteen-manage",
        component: () => import("@/views/canteen/Manage"),
        meta: {
          title: "商品管理(供)"
        }
      },
      {
        path: "statistics-producer",
        name: "Canteen-statistics-producer",
        component: () => import("@/views/canteen/StatisticsProducer"),
        meta: {
          title: "消费订单汇总查询(供)"
        }
      },
      {
        path: "order-detail-producer",
        name: "Canteen-order-producer",
        component: () => import("@/views/canteen/OrderDetailProducer"),
        meta: {
          title: "订单明细查询(供)"
        }
      },
      {
        path: "invoicing-report-producer",
        name: "Canteen-invocing-report-producer",
        component: () => import("@/views/canteen/InvoicingReportProducer"),
        meta: {
          title: "进销统计报表(供)"
        }
      }
    ]
  }
];
export const asyncRoutes = [
  {
    path: "/order",
    name: "Order",
    redirect: "/order/order-statistics",
    component: Layout,
    meta: {
      title: "订餐管理",
      icon: "el-icon-document"
    },
    children: [
      {
        path: "order-details",
        name: "Order-details",
        component: () => import("@/views/orderManage/Details"),
        meta: {
          title: "订餐明细"
        }
      }, {
        path: "order-statistics",
        name: "Order-statistics",
        component: () => import("@/views/orderManage/Statistics"),
        meta: {
          title: "订餐统计"
        }
      },
    ]
  },
  {
    path: "/subOrder",
    name: "subOrder",
    redirect: "/subOrder/subOrder-statistics",
    component: Layout,
    meta: {
      title: "订餐管理",
      icon: "el-icon-document"
    },
    children: [
      {
        path: "subOrder-details",
        name: "subOrder-details",
        component: () => import("@/views/orderManage/newDetails"),
        meta: {
          title: "订餐明细"
        }
      },
      {
        path: "subOrder-statistics",
        name: "subOrder-statistics",
        component: () => import("@/views/orderManage/newStatistics"),
        meta: {
          title: "订餐统计"
        }
      },
    ]
  },
  {
    path: "/settlement",
    name: "Settlement",
    redirect: "/settlement/details",
    component: Layout,
    meta: {
      title: "结算管理",
      icon: "el-icon-money"
    },
    children: [
      {
        path: "details",
        name: "Settlement-details",
        component: () => import("@/views/settlement/Details"),
        meta: {
          title: "消费明细"
        }
      }, {
        path: "statistics",
        name: "Settlement-statistics",
        component: () => import("@/views/settlement/Statistics"),
        meta: {
          title: "消费统计"
        }
      },
      {
        path: "details-account",
        name: "Settlement-details-account",
        component: () => import("@/views/settlement/Details/index_account.vue"),
        meta: {
          title: "消费明细(分账)"
        }
      }, {
        path: "statistics-account",
        name: "Settlement-statistics-account",
        component: () => import("@/views/settlement/Statistics/index_account.vue"),
        meta: {
          title: "消费统计(分账)"
        }
      },
    ]
  },
  {
    path: "/subSettlement",
    name: "SubSettlement",
    redirect: "/subSettlement/details2",
    component: Layout,
    meta: {
      title: "结算管理",
      icon: "el-icon-money"
    },
    children: [
      {
        path: "details2",
        name: "Settlement-details2",
        component: () => import("@/views/settlement/newDetails"),
        meta: {
          title: "消费明细"
        }
      }, {
        path: "statistics2",
        name: "Settlement-statistics2",
        component: () => import("@/views/settlement/newStatistics"),
        meta: {
          title: "消费统计"
        }
      },
      {
        path: "details2-account",
        name: "Settlement-details2-account",
        component: () => import("@/views/settlement/newDetails/index_account.vue"),
        meta: {
          title: "消费明细(分账)"
        }
      }, {
        path: "statistics2-account",
        name: "Settlement-statistics2-account",
        component: () => import("@/views/settlement/newStatistics/index_account.vue"),
        meta: {
          title: "消费统计(分账)"
        }
      },
    ]
  },
  {
    path: "/temperature",
    redirect: "/temperature/index",
    name: "Temperature",
    component: Layout,
    children: [
      {
        path: "index",
        name: "Temperature-index",
        component: () => import("@/views/temperature"),
        meta: {
          title: "体温监测",
          icon: "el-icon-sunny"
        }
      }
    ]
  },
  {
    path: "/payManage",
    redirect: "/payManage/index",
    name: "PayManage",
    component: Layout,
    children: [
      {
        path: "index",
        name: "PayManage-index",
        component: () => import("@/views/payManage"),
        meta: {
          title: "缴费管理",
          icon: "el-icon-bank-card"
        }
      }
    ]
  },
  {
    path: "/takeOut",
    redirect: "/takeOut/index",
    name: "TakeOut",
    component: Layout,
    children: [
      {
        path: "index",
        name: "TakeOut-index",
        component: () => import("@/views/takeout"),
        meta: {
          title: "外卖管理",
          icon: "el-icon-bicycle",
          // keepAlive: false 
        }
      }
    ]
  },
  {
    path: "/takeOut2",
    redirect: "/takeOut2/index",
    name: "TakeOut2",
    component: Layout,
    children: [
      {
        path: "index",
        name: "TakeOut2-index",
        component: () => import("@/views/takeout/newIndex.vue"),
        meta: {
          title: "外卖管理",
          icon: "el-icon-bicycle",
          // keepAlive: false 
        }
      }
    ]
  },
  {
    path: "/ticket",
    name: "Ticket",
    redirect: "/ticket/statistics",
    component: Layout,
    meta: {
      title: "工作餐管理",
      icon: "el-icon-money"
    },
    children: [
      {
        path: "statistics",
        name: "Ticket-statistics",
        component: () => import("@/views/receptionTicket/Statistics"),
        meta: {
          title: "工作餐查询"
        }
      },
      {
        path: "details",
        name: "Ticket-details",
        component: () => import("@/views/receptionTicket/Details"),
        meta: {
          title: "工作餐审批"
        }
      }
    ]
  },
  {
    path: "/recharge",
    name: "Recharge",
    redirect: "/recharge/cash",
    meta: {
      title: "充值管理",
      icon: "el-icon-bank-card"
    },
    component: Layout,
    children: [
      {
        path: "cash",
        name: "Recharge-cash",
        component: () => import("@/views/recharge/cash"),
        meta: {
          title: "现金充值与退款"
        }
      },
      {
        path: "record",
        name: "Recharge-record",
        component: () => import("@/views/recharge/record"),
        meta: {
          title: "充值记录明细"
        }
      },
      {
        path: "card-balance",
        name: "Recharge-card-balance",
        component: () => import("@/views/recharge/cardBalance"),
        meta: {
          title: "饭卡余额查询"
        }
      },
      {
        path: "record-statistic",
        name: "Recharge-record-statistic",
        component: () => import("@/views/recharge/recordStatistic"),
        meta: {
          title: "充值记录统计"
        }
      }
    ]
  },
  {
    path: "/recharge2",
    name: "Recharge-account",
    redirect: "/recharge2/cash2",
    meta: {
      title: "充值管理(分账)",
      icon: "el-icon-bank-card"
    },
    component: Layout,
    children: [
      {
        path: "cash2",
        name: "Recharge-cash-account",
        component: () => import("@/views/recharge_account/cash"),
        meta: {
          title: "现金充值与退款"
        }
      },
      {
        path: "record2",
        name: "Recharge-record-account",
        component: () => import("@/views/recharge_account/record"),
        meta: {
          title: "充值记录明细"
        }
      },
      {
        path: "card-balance2",
        name: "Recharge-card-balance-account",
        component: () => import("@/views/recharge_account/cardBalance"),
        meta: {
          title: "饭卡余额查询"
        }
      },
      {
        path: "record-statistic",
        name: "Recharge-record-statistic",
        component: () => import("@/views/recharge/recordStatistic"),
        meta: {
          title: "充值记录统计"
        }
      }
    ]
  },
  {
    path: "/consumerCard",
    redirect: "/consumerCard/index",
    name: "consumerCard",
    component: Layout,
    children: [
      {
        path: "index",
        name: "consumerCard-index",
        component: () => import("@/views/consumerCard"),
        meta: {
          title: "消费卡管理",
          icon: "el-icon-set-up"
        }
      }
    ]
  },
  {
    path: "/cuisine",
    name: "Cuisine",
    redirect: "/cuisine/settings",
    meta: {
      title: "菜品管理",
      icon: "el-icon-food"
    },
    component: Layout,
    children: [
      {
        path: "settings",
        name: "Cuisine-settings",
        component: () => import("@/views/cuisine/Settings"),
        meta: {
          title: "菜单设置"
        }
      },
      {
        path: "manage",
        name: "Cuisine-manage",
        component: () => import("@/views/cuisine/Manage"),
        meta: {
          title: "菜品管理"
        }
      },
      {
        path: "withoutManage",
        name: "Without-cuisine-manage",
        component: () => import("@/views/cuisine/ManageWithout"),
        meta: {
          title: "菜品管理(无选菜)"
        }
      }
    ]
  },
  {
    path: "/punishment",
    name: "Punishment",
    redirect: "/punishment/management",
    meta: {
      title: "惩罚管理",
      icon: "el-icon-document-delete"
    },
    component: Layout,
    children: [
      {
        path: "details",
        name: "Punishment-details",
        component: () => import("@/views/punishment/details"),
        meta: {
          title: "惩罚明细"
        }
      },
      {
        path: "management",
        name: "Punishment-management",
        component: () => import("@/views/punishment/management"),
        meta: {
          title: "惩罚管理"
        }
      },
      
      {
        path: "strategy",
        name: "Punishment-strategy",
        component: () => import("@/views/punishment/strategy"),
        meta: {
          title: "惩罚策略"
        }
      },
      {
        path: "editDetails",
        name: "Punishment-editDetails",
        component: () => import("@/views/punishment/editDetails"),
        meta: {
          title: "惩罚编辑详情"
        }
      }
    ]
  },
  {
    path: "/canteen",
    name: "Canteen",
    redirect: "/canteen/manage",
    component: Layout,
    meta: {
      title: "小卖部管理",
      icon: "el-icon-coffee"
    },
    children: [
      {
        path: "good-search",
        name: "Canteen-search",
        component: () => import("@/views/canteen/GoodSearch"),
        meta: {
          title: "商品查询(管)"
        }
      },
      {
        path: "order-detail-manager",
        name: "Canteen-order-manager",
        component: () => import("@/views/canteen/OrderDetailManager"),
        meta: {
          title: "订单明细查询(管)"
        }
      },
      {
        path: "statistics-manager",
        name: "Canteen-statistics-manager",
        component: () => import("@/views/canteen/StatisticsManager"),
        meta: {
          title: "消费订单汇总查询(管)"
        }
      },
      {
        path: "invoicing-report-manager",
        name: "Canteen-invocing-report-manager",
        component: () => import("@/views/canteen/InvoicingReportManager"),
        meta: {
          title: "进销统计报表(管)"
        }
      }
    ]
  },
  {
    path: "/material",
    name: "Material",
    redirect: "/material/details",
    meta: {
      title: "材料管理",
      icon: "el-icon-goods"
    },
    component: Layout,
    children: [
      {
        path: "detail",
        name: "Matertial-detail",
        component: () => import("@/views/material/Details"),
        meta: {
          title: "菜品材料明细"
        }
      },
      {
        path: "storage",
        name: "Material-storage",
        component: () => import("@/views/material/Storage"),
        meta: {
          title: "入库材料管理"
        }
      },
      {
        path: "order-material",
        name: "Material-Order",
        component: () => import("@/views/material/OrderMaterial"),
        meta: {
          title: "材料下单报表"
        }
      },
      {
        path: "price-details",
        name: "Material-price-details",
        component: () => import("@/views/material/PriceDetails"),
        meta: {
          title: "材料价格明细"
        }
      }
    ]
  },
  {
    path: "/settings",
    name: "Settings",
    redirect: "/settings/supplement",
    meta: {
      title: "设置",
      icon: "el-icon-setting"
    },
    component: Layout,
    children: [
      {
        path: "supplement",
        name: "Settings-supplement",
        component: () => import("@/views/settings/Supplement"),
        meta: {
          title: "补录管理"
        }
      },
      {
        path: "supplement2",
        name: "Settings-supplement-account",
        component: () => import("@/views/settings/Supplement_account"),
        meta: {
          title: "补录管理(分账)"
        }
      },
      {
        path: "department",
        name: "Settings-department",
        component: () => import("@/views/settings/Department"),
        meta: {
          title: "部门人员设置"
        }
      },
      {
        path: "consumption-strategy",
        name: "Settings-consumption-strategy",
        component: () => import("@/views/settings/ConsumptionStrategy"),
        meta: {
          title: "消费策略设置"
        }
      },
      {
        path: "consumption-strategy2",
        name: "Settings-consumption-strategy2",
        component: () => import("@/views/settings/ConsumptionStrategy/newIndex"),
        meta: {
          title: "消费策略设置"
        }
      },
      {
        path: "role",
        name: "Settings-role",
        component: () => import("@/views/settings/Role"),
        meta: {
          title: "角色设置"
        }
      },

      {
        path: "password",
        name: "Settings-password",
        component: () => import("@/views/settings/Password"),
        meta: {
          title: "密码设置"
        }
      },
      {
        path: "operation",
        name: "Settings-operation-journal",
        component: () => import("@/views/settings/OperationJournal"),
        meta: {
          title: "操作日志查询"
        }
      }
    ]
  },
  {
    path: "/cofigure",
    name: "Configure",
    component: Layout,
    meta: {
      title: "配置",
      icon: "el-icon-office-building"
    },
    children: [
      {
        path: "details",
        name: "Configuring-details",
        meta: {
          title: "企业明细"
        },
        component: () => import("@/views/configuring/EnterpriseDetails")
      },
      {
        path: "manage",
        name: "Configuring-manage",
        meta: {
          title: "企业管理"
        },
        component: () => import("@/views/configuring/EnterpriseManage")
      },
      {
        path: "consumer",
        name: "Configuring-consumer",
        meta: {
          title: "消费机配置"
        },
        component: () => import("@/views/configuring/Consumer")
      },
      {
        path: "account",
        name: "Configure-account",
        meta: {
          title: "账户信息管理"
        },
        component: () => import("@/views/configuring/accountManage")
      },
      {
        path: "outsiders",
        name: "Outsiders",
        component: () => import("@/views/settings/Outsiders"),
        meta: {
          title: '外来人员权限设置'
        }
      },
      {
        path: "modules",
        name: "Configuring-modules",
        meta: {
          title: "模块管理"
        },
        component: () => import("@/views/configuring/Modules")
      },
      {
        path: "modules-setting",
        name: "Configuring-modules-setting",
        meta: {
          title: "模块设置"
        },
        component: () => import("@/views/configuring/ModuleManage")
      }
    ]
  },
  {
    path: "/canteenSetting",
    name: "CanteenSetting",
    redirect: "/canteenSetting/wechat",
    component: Layout,
    meta: {
      title: "小卖部设置",
      icon: "el-icon-coffee"
    },
    children: [
      {
        path: "wechat",
        name: "CanteenSetting-wechat",
        component: () => import("@/views/canteenSetting/Wechat"),
        meta: {
          title: "微信端设置"
        }
      },
      {
        path: "supplier",
        name: "CanteenSetting-supplier",
        component: () => import("@/views/canteenSetting/SupplierManagement"),
        meta: {
          title: "供应商管理"
        }
      }
    ]
  },
  { path: "*", redirect: "/404", hidden: true }
];

const createRouter = () => {
  return new Router({
    routes: currencyRoutes,
    scrollBehavior() {
      return { x: 0, y: 0 };
    }
  });
};

const router = createRouter();
// 解决addRoute不能删除动态路由问题
export function resetRouter() {
  const reset = createRouter();
  router.matcher = reset.matcher;
}
router.beforeEach(async (to, from, next) => {
  //合并到正式环境时需修改
  const IS_HENGQIN=process.env.VUE_APP_ENV==='hengqin'  //是否为横琴环境
  // const IS_HENGQIN=true//测试横琴环境时用
  
  const IS_IPALLOW=await isAllow() //ip是否为白名单
  const BASE_ROUTE=to.path === "/debug"||to.path==="/debuglogin"||to.path === "/power"||to.path==='/login'||to.path === "/producer"
  const IS_LOGIN=store.getters.token //是否登陆过
  console.log(IS_HENGQIN,IS_IPALLOW,IS_UPGRADE,IS_LOGIN)

    if(BASE_ROUTE){
      // 如果转到无权限了 走你
      if(to.path==='/power'){
        next()
        console.log('没有权限1')
      }
      
      //如果在横琴但不是白名单 跳到无权限
      if(IS_HENGQIN&&!IS_IPALLOW){
        next('/power')
        console.log('没有权限2')
      }

      // 其他情况正常走
      next()
    }else {
      // 有权限：如果是横琴且ip为白名单 或者非横琴
      if((IS_HENGQIN&&IS_IPALLOW)||!IS_HENGQIN){
        if(IS_UPGRADE&&localStorage.getItem('goon')==null){
          next("/debug");
        }
        console.log('ok1')
        // 登陆过 有token
        if (IS_LOGIN) {
          console.log('有token，有权限，可以正常登录')
          let hasRoles = store.getters.roles.length > 0;
          if (hasRoles) {
            next();
          } else if (localStorage.isProducer) {
            const { roles } = await store.dispatch("user/_getUserModules");
            const addRoutes = await store.dispatch("permission/getAsyncRoutes",roles);
            router.addRoutes(addRoutes);
            next({ ...to, replace: true });
          } else {
            try {
              const { roles } = await store.dispatch("user/_getUserModules");
              const addRoutes = await store.dispatch(
                "permission/getAsyncRoutes",
                roles
              );
              router.addRoutes(addRoutes);
              next({ ...to, replace: true });
            } catch (error) {
              console.log(error);
            }
          }
        } else {
          console.log('ok2')
          if(IS_UPGRADE){
            next("/debug")
          }else{
            next("/login")
            console.log('有权限，可以正常登录')
          }
        }
      
      // 无权限
      }else{
        console.log('ok3')
        next('/power')
      }
    }
});

export default router;
