import $axios from './index';



// 设置--补录管理-单个充值
export function rechargeOnce(params) {
    const url = '/v1/wallet/supplement';
    return $axios.get(url, params)
}

// 设置--消费策略--修改消费策略
export function updateStrategy(data) {
    const url = '/v1/canteen/consumptionStrategy/update';
    return $axios.post(url, data);
}

// 设置--新增消费策略
export function addStrategy(data) {
    const url = '/v1/canteen/consumptionStrategy/save';
    return $axios.post(url, data)
}

//设置--获取饭堂消费策略设置
export function getCanteenStrategy(params) {
    const url = '/v1/canteen/consumptionStrategy';
    return $axios.get(url, params)
}