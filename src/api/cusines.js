import $axios from './index';


// 获取菜单设置列表
export function getMenus(params) {
    const url = '/v1/menus/company';
    return $axios.get(url, params);
};

// 菜品列表
export function getCusines(params) {
    const url = '/v1/foods';
    return $axios.get(url, params);
}

// 新增编辑饭堂菜单
export function updateMenu(data) {
    const url = '/v1/menu/save';
    return $axios.post(url, data)
}

// 获取指定饭堂菜单信息
export function getMenuInfo(params) {
    const url = '/v1/menus/canteen';
    return $axios.get(url, params)
}

// 获取菜品信息
export function getCusineInfo(params) {
    const url = '/v1/food';
    return $axios.get(url, params)
}

// 菜品状态设置(删除）
export function deleteCusine(data) {
    const url = '/v1/food/handel';
    return $axios.post(url, data)
}

// 菜品管理-修改菜品
export function updateCusine(data) {
    const url = '/v1/food/update';
    return $axios.post(url, data)
}

// 菜品管理-新增菜品
export function addCusine(data) {
    const url = '/v1/food/save';
    return $axios.post(url, data)
}