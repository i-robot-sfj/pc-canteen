import $axios from './index';

// 企业管理-企业列表
export function getManageCompany(params) {
    const url = '/v1/manager/companies';
    return $axios.get(url, params);
}

// 企业管理-获取设备列表（小卖部/饭堂）
export function getManageMachine(params) {
    const url = '/v1/machines';
    return $axios.get(url, params);
}

// 企业管理--添加企业微信支付配置信息
export function addPayInfo(data) {
    const url = '/v1/company/wxConfig/save';
    return $axios.post(url, data)
}

//-企业管理-获取指定企业消费地点
export function getConsumptionLocation(params) {
    const url = '/v1/company/consumptionLocation';
    return $axios.get(url, params)
}

// 企业管理-获取非企业人员二维码
export function getOutdersQrcode(params) {
    const url = '/v1/company/qrcode'
    return $axios.get(url, params)
}

// 企业管理-修改硬件信息(饭堂和小卖部)
export function updataMachine(data) {
    const url = '/v1/canteen/updateMachine'
    return $axios.post(url, data)
}

// 企业管理-小卖部删除
export function deleteShop(data) {
    const url = '/v1/shop/delete'
    return $axios.post(url, data)
}
// 企业管理-新增小卖部
export function deleteShop(data) {
    const url = '/v1/shop/save'
    return $axios.post(url, data)
}
// 企业管理-更新小卖部
export function updateShop(data) {
    const url = '/v1/shop/save'
    return $axios.post(url, data)
}

// 企业管理-添加硬件(饭堂/小卖部)
export function updateShop(data) {
    const url = '/v1/canteen/saveMachine'
    return $axios.post(url, data)
}

// 企业管理--删除饭堂餐次
export function updateShop(data) {
    const url = '/v1/canteen/dinner/delete'
    return $axios.post(url, data)
}

// 企业管理--新增企业
export function addNewCompany(data) {
    const url = '/v1/company/save'
    return $axios.post(url, data)
}

// 企业管理--新增饭堂
export function addCanteen(data) {
    const url = '/v1/canteen/save'
    return $axios.post(url, data)
}

// 企业管理--新增饭堂配置信息
export function addCanteenInfo(data) {
    const url = '/v1/canteen/configuration/save';
    return $axios.post(url, data)
}
// 企业管理--更新饭堂配置信息
export function updateCanteenInfo(data) {
    const url = '/v1/canteen/configuration/update';
    return $axios.post(url, data)
}

// 企业管理--获取饭堂设置信息
export function getCanteenInfo(params) {
    const url = '/v1/canteen/configuration';
    return $axios.get(url, params)
}