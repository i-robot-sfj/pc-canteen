import $axios from './index';

// 供应商获取商品列表-查询接口
export function getSupplierProductsOptions(params) {
    const url = '/v1/shop/supplierProducts/search';
    return $axios.get(url, params)
}
// 选择饭堂查看餐次列表
export function getDinnersOptions(params) {
    const url = '/v1/canteen/dinners';
    return $axios.get(url, params);
}

// 获取企业下饭堂列表
export function getCanteensOptions(params) {
    const url = '/v1/consumption/place';
    return $axios.get(url, params);
}

// 获取获取企业商品列表-查询接口
export function searchProduct(params) {
    const url = '/v1/shop/companyProducts/search';
    return $axios.get(url, params);
}

// 系统管理员/企业超级管理员/企业管理员--查看可见企业
export function getCompanies(params) {
    const url = '/v1/admin/companies';
    return $axios.get(url, params);
}

// 获取指定餐次菜单信息（查询类接口）
export function getMenusOptions(params) {
    const url = '/v1/menus/dinner'
    return $axios.get(url, params);
}

// 获取当前角色可管理饭堂列表
export function getManageCanteens(params) {
    const url = '/v1/managerCanteens'
    return $axios.get(url, params);
}

// 获取企业部门列表
export function getDepartments(parmas) {
    const url = '/v1/departments';
    return $axios.get(url, parmas);
}

//获取体温界面表单数据
export function getData(parmas) {
    const url = '/v1/face/getFaceData';
    return $axios.get(url, parmas);
}

//体温界面输出表单
export function exportFile(parmas) {
    const url = '/v1/face/exportFaceData';
    return $axios.get(url, parmas);
}
//获取账户信息
export function getAccounts(parmas) {
    const url = '/v1/accounts/search';
    return $axios.get(url, parmas);
}

// 企业明细-查看饭堂信息
export function getCanteens(params) {
    const url = '/v1/canteens';
    return $axios.get(url, params)
}

// 获取部门
export function getAdminDepartment(params) {
    const url = '/v1/admin/departments';
    return $axios.get(url, params)
}

// 获取角色
export function getRoleType(params) {
    const url = '/v1/role/types';
    return $axios.get(url, params)
}

