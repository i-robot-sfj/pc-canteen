import $axios from "./index";

export function getPageTab1(params) {
  const url = "/v1/getPageData1";
  return $axios.get(url, params);
}
export function getPageTab2() {
  const url = "/v1/getPageData2";
  return $axios.get(url);
}
