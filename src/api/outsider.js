import $axios from './index';

// 外来人员设置 - 企业配置列表
export function getOutsidersList(params) {
    const url = '/v1/outsiders';
    return $axios.get(url, params)
}
// 外来人员设置 - 企业配置信息
export function getOutsidersInfo(params) {
    const url = '/v1/outsider';
    return $axios.get(url, params)
}
// 外来人员设置 - 外来人员设置-饭堂/权限编辑
export function getOutsidersInfo(data) {
    const url = '/v1/outsider/save';
    return $axios.post(url, data)
}
