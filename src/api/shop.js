import $axios from './index';

// 小卖部管理-商品管理-企业账号获取商品列表
export function getAdminProducts(params) {
    const url = '/v1/shop/cms/products';
    return $axios.get(url, params);
}

// 小卖部管理-商品管理-供应商获取商品列表
export function getSupplierProducts(params) {
    const url = '/v1/shop/supplier/products';
    return $axios.get(url, params);
}

// 小卖部管理-商品管理-获取该企业下供货商列表
export function getNowSuppliers(params) {
    const url = '/v1/company/suppliers';
    return $axios.get(url, params);
}

// 小卖部设置-新增供应商
export function deleteSupplier(data) {
    const url = '/v1/supplier/save';
    return $axios.post(params, data);
}

// 小卖部设置-删除供应商
export function deleteSupplier(data) {
    const url = '/v1/supplier/delete';
    return $axios.post(params, data);
}

// 小卖部设置-更新供应商
export function updateeSupplier(data) {
    const url = '/v1/supplier/update';
    return $axios.post(params, data);
}

// 小卖部设置-新增商品类型
export function saveCategory(data) {
    const url = '/v1/category/save';
    return $axios.post(params, data);
}

// 小卖部设置-删除商品类型
export function deleteCategory(data) {
    const url = '/v1/category/delete';
    return $axios.post(params, data);
}

// 小卖部设置-删除商品类型
export function updateCategory(data) {
    const url = '/v1/category/update';
    return $axios.post(params, data);
}

// 小卖部设置-供应商列表
export function getAllSupplier(params) {
    const url = '/v1/suppliers';
    return $axios.get(url, params);
}

// 小卖部设置-商品类型列表
export function getCategory(params) {
    const url = '/v1/categories';
    return $axios.get(url, params);
}
// 小卖部设置-商品管理-获取商品信息
export function getProductInfo(params) {
    const url = '/v1/shop/product';
    return $axios.get(url, params);
}

// 小卖部管理-商品管理-修改商品(只有供应商才有权限)
export function updateProduct(data) {
    const url = '/v1/shop/product/update';
    return $axios.post(url, data);
}
// 小卖部管理-商品管理-商品入库(供应商才有权限)
export function saveStocks(data) {
    const url = '/v1/shop/stock/save';
    return $axios.post(url, data);
}
// 小卖部管理-商品管理-新增商品
export function saveProduct(data) {
    const url = '/v1/shop/product/save';
    return $axios.post(url, data);
}

// 小卖部管理-商品管理-商品状态操作(只有供应商才有权限)
export function handleProduct(data) {
    const url = '/v1/shop/product/handel';
    return $axios.post(url, data);
}

// 小卖部管理-消费订单汇总查询-供应商/管理员
export function getOrderConsumption(params) {
    const url = '/v1/shop/orderConsumption';
    return $axios.get(url, params);
}

// 小卖部管理-订单明细查询-供应商
export function getSupplierOrder(params) {
    const url = '/v1/shop/order/statistic/supplier'
    return $axios.get(url, params);
}

// 小卖部管理-订单明细查询-管理员
export function getmanagerOrder(params) {
    const url = '/v1/shop/order/statistic/manager'
    return $axios.get(url, params);
}

// 小卖部管理-订单明细查询-订单明细
export function getOrderInfo(params) {
    const url = '/v1/shop/order/products';
    return $axios.get(url, params);
}

// 小卖部管理-进销报表-供应商
export function getSupllierSaleReport(params) {
    const url = '/v1/shop/salesReport/supplier';
    return $axios.get(url, params)
}

// 小卖部管理-进销报表-管理员
export function getManageSaleReport(params) {
    const url = '/v1/shop/salesReport/manager';
    return $axios.get(url, params)
}