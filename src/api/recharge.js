import $axios from './index';

// 充值管理--企业员工列表
export function getStaffs(params) {
    const url = '/v1/department/staffs/recharge';
    return $axios.get(url, params);
}

// 充值管理-充值记录明细-充值人员列表
export function getStaffsList(params) {
    const url = '/v1/wallet/recharge/admins';
    return $axios.get(url, params);
}

// 充值管理-充值记录列表
export function getRecords(params) {
    const url = '/v1/recharges';
    return $axios.get(url, params);
}

// -充值管理--现金充值
export function rechargeCash(data) {
    const url = '/v1/wallet/recharge/cash';
    return $axios.post(url, data)
}

//充值管理--饭卡余额查询-一键清零
export function clearBalance(data) {
    const url = '/v1/wallet/clearBalance'
    return $axios.post(url, data)
}
//注意：饭卡余额查询页面之前的前端做页面时没有调用这些接口，需要去页面修改
//充值管理-饭卡余额查询
export function getBalanceRecord(params) {
    const url = '/v1/wallet/users/balance'
    return $axios.get(url, params)
}

// 充值管理-获取用户可以查看部门列表
export function getRechargeDeparment(params) {
    const url = '/v1/departments/recharge'
    return $axios.get(url, params)
}

// 充值管理-充值统计
export function getRechargeStatistic(params) {
    const url = '/v1/wallet/rechargeStatistic'
    return $axios.get(url, params)
}

//充值管理-饭卡余额查询（某天的实际余额和可用余额）,2022年3月23日
export function getRechargeMoney(params) {
    const url = '/v3/wallet/users/balance/day/export'
    return $axios.post(url, params)
}

//充值管理-饭卡余额查询（日期），2022年3月23日
export function getRechargeDate(params) {
    const url = '/v3/wallet/users/balance/day'
    return $axios.get(url, params)
}