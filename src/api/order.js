import $axios from "./index";

// 订餐管理-订餐明细
export function getOrderDetailsList(params) {
    const url = '/v1/order/orderStatistic/detail'
    return $axios.get(url, params)
}

// 订餐管理-订餐明细-子菜单
export function getOrderDetailsSubList(params) {
    const url = '/v1/order/orderStatistic/detail/info'
    return $axios.get(url, params)
}

// 订餐管理-订餐明细-子菜单
export function getOrderSubDetails(params) {
    const url = '/v1/order/detail'
    return $axios.get(url, params)
}

//订餐管理-订餐明细-取消订餐
export function goCancelOrder(data) {
    const url = '/v1/order/cancel/manager'
    return $axios.post(url, data)
}
//订餐管理-订餐统计
export function getOrderStatistic(params) {
    const url = '/v1/order/orderStatistic'
    return $axios.get(url, params)
}