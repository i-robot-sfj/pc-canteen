import $axios from "./index";

// 获取角色类型
export function getRoleTypes(params) {
  const url = '/role/types'
  return $axios.get(url, params)
};

// 设置-角色列表
export function getRolesList(params) {
  const url = '/roles';
  return $axios.get(url, params)
}

// 新增角色信息
export function addRole(data) {
  const url = '/v1/role/save'
  return $axios.post(url, data)
}

// 修改角色信息
export function updateRole(data) {
  const url = '/v1/role/update'
  return $axios.post(url, data)
}

// 角色状态修改
export function handleRole(data) {
  const url = '/v1/role/handel'
  return $axios.post(url, data)
}

// 设置-角色模块设置-获取角色信息
export function getRoleInfo(params) {
  const url = '/v1/role';
  return $axios.get(url, params)
}

// 新增角色类型
export function addRoleType(data) {
  const url = '/v1/role/type/save'
  return $axios.post(url, data)
}

// 修改角色类型信息
export function updateRoleType(data) {
  const url = '/v1/role/type/update'
  return $axios.post(url, data)
}

// 获取用户可查看饭堂信息
export function getRoleCanteen(params) {
  const url = '/v1/canteens/role';
  return $axios.get(url, params)
}