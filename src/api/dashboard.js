import $axios from "./index";

export function getCardsData() {
  const url = "/v1/getCardsData";
  return $axios.get(url);
}
export function getLineData() {
  const url = "/v1/getLineData";
  return $axios.get(url);
}
export function getTableData() {
  const url = "/v1/getTableList";
  return $axios.get(url);
}
export function getBarData() {
  const url = "/v1/getBarData";
  return $axios.get(url);
}
