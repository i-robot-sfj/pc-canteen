import $axios from './index';

// 获取菜品材料明细列表
export function getMaterialDeails(params) {
    const url = '/v1/materials'
    return $axios.get(url, params)
};
// 材料管理-菜品材料明细
export function getFoodDeails(params) {
    const url = '/v1/materials/food';
    return $axios.get(url, params)
}

//菜品材料明细-编辑菜品材料明细
export function updateMaterailDetail(data) {
    const url = '/v1/food/material/update';
    return $axios.post(url, data)
}

// 材料管理-入库材料报表-列表
export function getReportList(params) {
    const url = '/v1/order/material/reports'
    return $axios.get(url, params)
}

// 材料管理-入库材料报表-报表详情
export function getReportMaterialsInfo(params) {
    const url = '/v1/order/material/report';
    return $axios.get(url, params)
}

//材料管理-材料下单表
export function getOrderMaterials(params) {
    const url = '/v1/order/materialsStatistic'
    return $axios.get(url, params)
}

//材料管理-材料下单表-提交生成报表
export function updateMaterails(data) {
    const url = '/v1/order/material/update'
    return $axios.post(url, data)
}

// 材料管理-入库材料报表-废除
export function deleteReport(data) {
    const url = '/v1/order/material/report/delete';
    return $axios.post(url, data)
}

// 材料价格明细-新增价格明细
export function addMaterailDetail(data) {
    const url = '/v1/material/save';
    return $axios.post(url, data)
}

// 材料价格明细-更新价格明细
export function updateMaterailPriceDetail(data) {
    const url = '/v1/material/update';
    return $axios.post(url, data)
}

// 材料价格明细-删除价格明细
export function deleteMaterailDetail(data) {
    const url = '/v1/material/handel';
    return $axios.post(url, data)
}

// 材料管理-材料下单表-修改订单地址
export function changeAddress(data) {
    const url = '/v1/order/changeAddress';
    return $axios.post(url, data)
}



// 版本2
// 材料管理-菜品材料明细-添加材料
export function addFoodMaterial(data) {
    const url = '/v2/food/material/save';
    return $axios.post(url, data)
}

// 材料管理-菜品材料明细-编辑材料
export function updateFoodMaterial(data) {
    const url = '/v2/food/material/update';
    return $axios.post(url, data)
}

// 材料管理-入库材料管理-获取列表
export function getStoreSheets(params) {
    const url = '/v2/material/order/reports'
    return $axios.get(url, params)
}

// 材料管理-入库材料管理-报表详情
export function getStoreSheetDetail(params) {
    const url = '/v2/material/order/report/detail'
    return $axios.get(url, params)
}

// 材料管理-材料明细下单表-获取列表
export function getMaterialSheets(params) {
    const url = '/v2/material/order/list'
    return $axios.get(url, params)
}

// 材料管理-材料明细下单表-添加材料
export function addMaterial(data) {
    const url = '/v2/material/order/save';
    return $axios.post(url, data)
}

// 材料管理-材料明细下单表-删除材料
export function deleteMaterial(data) {
    const url = '/v2/material/order/delete';
    return $axios.post(url, data)
}

// 材料管理-材料明细下单表-更新材料
export function updateMaterial(data) {
    const url = '/v2/material/order/update';
    return $axios.post(url, data)
}

// 材料管理-材料明细下单表-提交报表
export function submitSheet(data) {
    const url = '/v2/material/order/report';
    return $axios.post(url, data)
}

// 材料管理-入库材料管理-作废报表
export function deleteSheet(data) {
    const url = '/v2/material/order/report/cancel';
    return $axios.post(url, data)
}