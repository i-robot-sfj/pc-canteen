import $axios from './index';
import exportFiles from "@/utils/exportFile";
import { Message } from "element-ui";

// 充值管理-充值记录列表-导出报表
export async function exportRecharge(params) {
    const url = '/v1/wallet/recharges/export',
    const res = await $axios.get(url, params);
    if (res.msg === "ok") {
        exportFiles(res.data.url);
    } else {
        Message.error(res.msg);
    }
}

// 外卖管理-订单列表-导出报表
export async function exportRecharge(params) {
    const url = '/v1/order/takeoutStatistic/export',
    const res = await $axios.get(url, params);
    if (res.msg === "ok") {
        exportFiles(res.data.url);
    } else {
        Message.error(res.msg);
    }
}



