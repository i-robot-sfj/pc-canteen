import $axios from './index';

// 企业明细-企业列表
export function getChildCompanies(params) {
    const url = '/v1/companies';
    return $axios.get(url, params);
}

// 企业明细-获取企业设备列表
export function getCompanyMachine(params) {
    const url = '/v1/machines/company';
    return $axios.get(url, params);
}

// 企业明细-查看饭堂信息
export function getCanteenInfo(params) {
    const url = '/v1/canteens/company';
    return $axios.get(url, params)
}
// 企业明细-删除设备
export function deleteMachine(data) {
    const url = '/v1/canteen/deleteMachine';
    return $axios.post(url, data)
}