import $axios from './index';

// 外卖管理-订单列表
export function getTakoutList(params) {
    const url = '/v1/order/takeoutStatistics';
    return $axios.get(url, params)
}

// 外卖管理--获取打印订单的信息
export function getOrderPrintInfo(params) {
    const url = '/v1/order/info/print'
    return $axios.get(url, params)

}

// 外卖管理-接单操作
export function receiveOrder(data) {
    const url = '/v1/order/receive';
    return $axios.post(url, data)
}

// 外卖管理-订单操作6.20
export function order_handel(data) {
    const url = '/api/v1/order/handel';
    return $axios.post(url, data)
}
