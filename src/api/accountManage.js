import $axios from './index';

//新增账户
export function saveAccount(data) {
    const url = "/v1/account/save";
    return $axios.post(url, data)
}
//编辑账户
export function updateAccount(data) {
    const url = "/v1/account/update"
    return $axios.post(url, data)
}
//获取账户信息
export function getAccount(params) {
    const url = "/v1/account";
    return $axios.get(url, params)

}
// 企业账户状态操作
export function handleStatus(data) {
    const url = "/v1/account/handle"
    return $axios.post(url, data)
}
//扣费顺序查询你
export function getSortLists(params) {
    const url = "/v1/account/staffs";
    return $axios.get(url, params)

}
