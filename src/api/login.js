import $axios from "./index";
import $axiosnew from "./newIndex";

// 企业用户登录
export function login(data) {
  const url = "/v2/token/admin";
  return $axiosnew.post(url, data);
}

// 供应商登录
export function producerLogin(data) {
  const url = "/v1/token/supplier";
  return $axios.post(url, data);
}

// -登录成功后获取模块权限列表
export function getUserModules(params) {
  const url = "/v1/modules/admin";
  return $axios.get(url, params);
}

// 修改账号密码
export function updatePsw(data) {
  const url = "/v1/role/passwd/update";
  return $axios.post(url, data);
}
//获取消费方式
export function getConsumptionType(params) {
  const url = "/v1/company/consumptionType";
  return $axios.get(url, params);
}

//获取导出记录
export function getExportLogs(params) {
  const url = "/v1/excels";
  return $axios.get(url, params);
}


//删除导出记录
export function deleteExportLogs(data) {
  const url = "/v1/excel/delete";
  return $axios.post(url, data);
}

//获取验证码
export function getCode(params) {
  const url = "/v1/token/verify";
  return $axios.get(url);
}