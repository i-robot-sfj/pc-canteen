import $axios from './index';

// 结算管理-消费明细
export function getOrderSettlement(params) {
    const url = '/v1/order/orderSettlement';
    return $axios.get(url, params)
}

// 结算管理-结算报表
export function getStatistic(params) {
    const url = '/v1/order/consumptionStatistic';
    return $axios.get(url, params)
}

// 结算管理-结算报表
export function getNewStatistic(params) {
    const url = '/v2/order/consumptionStatistic';
    return $axios.get(url, params)
}