import $axios from './index';

// 企业员工列表
export function getStaffs(params) {
    const url = '/v1/staffs';
    return $axios.get(url, params);
}


// 获取企业部门列表
export function getDepartments(parmas) {
    const url = '/v1/departments';
    return $axios.get(url, parmas);
}

// 删除员工
export function deleteStaff(data) {
    const url = '/v1/department/staff/delete';
    return $axios.post(url, data)
}

// 新增员工
export function addStaff(data) {
    const url = '/department/staff/save';
    return $axios.post(url, data)
}
// 编辑员工
export function updateStaff(data) {
    const url = '/department/staff/update';
    return $axios.post(url, data)
}

// 生成员工二维码
export function saveQrcode(data) {
    const url = '/staff/qrcode/save'
    return $axios.post(url, data)
}

// 移动员工部门
export function moveStaff(data) {
    const url = '/department/staff/move'
    return $axios.post(url, data)
}

// 设置--部门管理--删除企业部门
export function deleteDepartment(data) {
    const url = '/department/delete'
    return $axios.post(url, data)
}
// 设置--部门管理--新增企业部门
export function addDepartment(data) {
    const url = '/department/save'
    return $axios.post(url, data)
}
// 设置--部门管理--更新企业部门
export function addDepartment(data) {
    const url = '/department/save'
    return $axios.post(url, data)
}