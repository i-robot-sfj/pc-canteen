//惩罚机制
import $axios from './index';

// 惩罚明细-获取惩罚明细
export function getDetails(params) {
    const url = '/v1/punishment/penaltyDetails';
    return $axios.get(url, params)
}

// 惩罚策略-获取惩罚策略
export function getStrategy(params) {
    const url = '/v1/punishment/strategyDetail';
    return $axios.get(url, params)
}

// 惩罚策略-修改惩罚策略
export function changeStrategy(data) {
    const url = '/v1/punishment/updateStrategy';
    return $axios.post(url, data)
}

// 惩罚管理-员工状态列表
export function getStaffState(params) {
    const url = '/v1/punishment/getPunishmentStaffInfo';
    return $axios.get(url, params)
}

// 惩罚管理-修改状态
export function changeState(data) {
    const url = '/v1/punishment/updatePunishmentStatus';
    return $axios.post(url, data)
}

// 惩罚管理-编辑状态-获取员工类型最大违规数
export function getCount(data) {
    const url = '/v1/punishment/getStaffMaxPunishment';
    return $axios.get(url, data)
}

// 惩罚编辑详情-获取列表
export function getDetailInfo(data) {
    const url = '/v1/punishment/getPunishmentEditDetails';
    return $axios.get(url, data)
}