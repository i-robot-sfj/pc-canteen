import $axios from './index';

// 模块管理-修改企业模块
export function updateCompanyModules(data) {
    const url = '/v1/module/company/update';
    return $axios.post(url, data);
}

// 模块管理-修改系统模块
export function updateCompanyModules(data) {
    const url = '/v1/module/update';
    return $axios.post(url, data);
}

// 模块管理--功能模块属性修改
export function updateSystemModules(data) {
    const url = '/v1/canteen/module/category';
    return $axios.post(url, data);
}
// 模块管理--功能模块状态操作
export function handleSystemModules(data) {
    const url = '/v1/module/system/handel';
    return $axios.post(url, data);
}

// 模块管理--新增系统功能模块
export function addSystemModules(data) {
    const url = '/v1/module/system/canteen/save';
    return $axios.post(url, data)
}

// 模块管理--系统模块-默认模块状态
export function handleDefalutModules(data) {
    const url = '/v1/module/default/handel';
    return $axios.post(url, data)
}

// 获取企业功能模块(不包括系统所有模块)
export function getWithoutSystemModules(params) {
    const url = '/v1/modules/canteen/withoutSystem';
    return $axios.get(url, params)
}

// 获取企业模块功能模块:包括系统模块
export function getWithSystemModules(params) {
    const url = '/v1/modules/canteen/withSystem';
    return $axios.get(url, params)
}

// 获取系统模块
export function getSystemModules(params) {
    const url = '/v1/modules';
    return $axios.get(url, params)
}