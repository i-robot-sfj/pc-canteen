import $axios from './index';

// 接待票-获取接待票申请列表
export function receptionsForApply(params) {
    const url = '/v1/reception/receptionsForApply'
    return $axios.get(url, params)
}